import {
  moduleName,
  SETTING_PREFIX,
  ANIMATION,
  debug,
} from '../const.js';

export class animationFilesConfiguration extends FormApplication {

  // get the default options
  static get defaultOptions() {
    return mergeObject(super.defaultOptions, {
      id: 'animationTemplateTitle-form',
      title: game.i18n.localize(SETTING_PREFIX + 'animationFilesPage.title'),
      template: './modules/' + moduleName + '/scripts/configuration/templates/animationFilesTemplate.html',
      closeOnSubmit: false
    });
  }

  // get the data for the page
  getData() {
    // Get boilerplate text phrases.
    let hint = game.i18n.localize(SETTING_PREFIX + 'animationFileHint');
    let credit = game.i18n.localize(SETTING_PREFIX + 'animationFileCredit');
    let unknown = game.i18n.localize(SETTING_PREFIX + 'animationFileCreditUnknown');
    let weapons = game.i18n.localize(SETTING_PREFIX + 'weapons');

    let settingList = [];
    Object.keys(ANIMATION).forEach(key => {
      // Get animation related phrases.
      let animation = ANIMATION[key];
      let setting = animation.setting;
      let name = game.i18n.localize(SETTING_PREFIX + setting);
      let source = animation.source ? animation.source : unknown;
      debug('Animation setting ' + setting);
      // Concatenate phrases to the proper template variables.
      settingList.push({
        animationTemplatesTitle: name[0].toUpperCase() + name.substring(1),
        animationTemplatesName: setting,
        animationTemplatesHint: hint + name + ' ' + weapons + ' (' + credit + source + ').',
        animationTemplatesFile: game.settings.get(moduleName, setting)
      });
    });

    var animationList = { animationTemplatesList : settingList };
    return animationList;
  }

  //create the listeners
  activateListeners(html) {
    //call the super listeners
    super.activateListeners(html);
    let ventana = this;
    ventana.render(true);
  }

  // update the object (create a new template)
  async _updateObject(event, formData) {
    //get the saved config data
    await game.settings.set(moduleName, formData.animationFileName, formData.animationFile.replaceAll("'",""));
    let ventana = this;
    ventana.render(true);
    //ui.notifications.info(game.i18n.localize("SWADESFXVFX.importingAnimationTemplate"));
  }
}
