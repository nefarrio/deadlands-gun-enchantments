import {
    moduleName,
    SETTING_PREFIX,
    WEAPON,
    ANIMATION,
    BLOOD,
    debug,
} from '../const.js';
import {
    animationFilesConfiguration
} from './animationFilesConfiguration.js';
import {
    gunTypesConfiguration
} from './gunTypesConfiguration.js';
import {
    bloodSettingsConfiguration
} from './bloodSettingsConfiguration.js';

function registerArraySettings(settingArr) {
  //const debouncedReload = foundry.utils.debounce(window.location.reload, 100);

  Object.keys(settingArr).forEach(key => {
    let setting = settingArr[key].setting;
    debug('registering setting ' + setting);
    game.settings.register(moduleName, setting, {
      name: SETTING_PREFIX + setting + '.name',
      hint: SETTING_PREFIX + setting + '.hint',
      scope: 'world',
      config: false,
      default: settingArr[key].default,
      type: String
      //onChange: debouncedReload
    });
  });
}

export function registerSettings() {
    // Animation settings
    registerArraySettings(ANIMATION);
    // Weapon settings
    registerArraySettings(WEAPON);
    // Blood settings
    registerArraySettings(BLOOD);

    // Menus
    game.settings.registerMenu(moduleName, 'deadlandsEnchantmentsAnimationFilesMenu', {
        name: SETTING_PREFIX + 'animationFiles.name',
        label: SETTING_PREFIX + 'animationFiles.label',
        hint: SETTING_PREFIX + 'animationFiles.hint',
        icon: "fas fa-edit", // A Font Awesome icon used in the submenu button
        type: animationFilesConfiguration,
        restricted: true
    });

    game.settings.registerMenu(moduleName, 'deadlandsEnchantmentsGunTypesMenu', {
        name: SETTING_PREFIX + 'gunTypes.name',
        label: SETTING_PREFIX + 'gunTypes.label',
        hint: SETTING_PREFIX + 'gunTypes.hint',
        icon: "fas fa-edit", // A Font Awesome icon used in the submenu button
        type: gunTypesConfiguration,
        restricted: true
    });

    game.settings.registerMenu(moduleName, 'deadlandsEnchantmentsBloodSettingsMenu', {
        name: SETTING_PREFIX + 'bloodSettings.name',
        label: SETTING_PREFIX + 'bloodSettings.label',
        hint: SETTING_PREFIX + 'bloodSettings.hint',
        icon: "fas fa-edit", // A Font Awesome icon used in the submenu button
        type: bloodSettingsConfiguration,
        restricted: true
    });

    // Configurable features.
    let setting = 'scream';
    debug('registering setting ' + setting);
    game.settings.register(moduleName, setting, {
      name: SETTING_PREFIX + setting + '.name',
      hint: SETTING_PREFIX + setting + '.hint',
      scope: 'world',
      config: true,
      default: true,
      type: Boolean
    });

    setting = 'commaCheck';
    debug('registering setting ' + setting);
    game.settings.register(moduleName, setting, {
      name: SETTING_PREFIX + setting + '.name',
      hint: SETTING_PREFIX + setting + '.hint',
      scope: 'world',
      config: true,
      default: true,
      type: Boolean
    });
}

export function checkForCommas(dict) {
  if (game.settings.get(moduleName, 'commaCheck')) {
    let found = false;
    Object.values(dict).forEach(el => {
      if (el.setting.includes(',')) {
        found = true;
      }
    });

    if (found) {
      ui.notifications.info(game.i18n.localize('DeadlandsEnchantments.CommaWarning'));
    }
  }
}

