import {
  moduleName,
  SETTING_PREFIX,
  debug,
} from '../const.js';
import {
  initBlood,
} from '../blood.js';
import {
  DEADLANDS_BLOOD,
} from '../init.js';


export class bloodSettingsConfiguration extends FormApplication {

  // get the default options
  static get defaultOptions() {
    return mergeObject(super.defaultOptions, {
      id: 'bloodSettingsTitle-form',
      title: game.i18n.localize(SETTING_PREFIX + 'bloodSettingsPage.title'),
      template: './modules/' + moduleName + '/scripts/configuration/templates/bloodSettingsTemplate.html',
      closeOnSubmit: false
    });
  }

  // get the data for the page
  getData() {
    // Init blood dictionary to update it in case new blood types were added.
    initBlood();

    // Get boilerplate text phrases.
    let hint = game.i18n.localize(SETTING_PREFIX + 'bloodHint');
    let separated = game.i18n.localize(SETTING_PREFIX + 'separated');

    let settingList = [];
    Object.values(DEADLANDS_BLOOD).forEach(blood => {
      // The default blood type is bypassed.
      if (blood.bypassConfig) {
        return;
      }

      // Get animation related phrases.
      let setting = blood.settingName;
      let title = game.i18n.localize(SETTING_PREFIX + setting);
      let bloodPhrase = title.toLowerCase()
      debug('Blood setting ' + setting);
      // Concatenate phrases to the proper template variables.
      settingList.push({
        bloodTypesTitle: title,
        bloodTypesName: setting,
        bloodTypesHint: hint + bloodPhrase + separated,
        bloodTypesCreatures: game.settings.get(moduleName, setting)
      });
    });

    var bloodTypes = { bloodTypesList : settingList };

    return bloodTypes;
  }

  //create the listeners
  activateListeners(html) {
    //call the super listeners
    super.activateListeners(html);
    let element = 'bloodTypesCreatures';

    document.getElementById(element).addEventListener("drop", async (event) => {
      let data;
      try {
        data = JSON.parse(event.dataTransfer.getData("text/plain"));
        let creatureName;
        if (data.type === "Actor" || data.type === "actor") {
          if (data.pack) {
            //from compendium
            let compendium = await game.packs.get(data.pack).getIndex();
            creatureName = compendium.get(data.id).name
          } else {
            //from actors
            creatureName = game.actors.get(data.id).name;
          }
          document.getElementById(element).value = document.getElementById(element).value +
          (document.getElementById(element).value.trim() == "" ? "" : ",") + creatureName;
        }
      } catch (err) {
        console.error(err);
      }
    });

    let ventana = this;
    ventana.render(true);
  }

  // update the object
  async _updateObject(event, formData) {
    //get the saved config data
    await game.settings.set(moduleName, formData.bloodTypesName, formData.bloodTypesCreatures.replaceAll("'",""));
    let ventana = this;
    ventana.render(true);
    //ui.notifications.info(game.i18n.localize("SWADESFXVFX.importingAnimationTemplate"));
  }
}
