import {
    registerSettings,
} from './configuration/settings.js';
import {
    initBlood,
} from './blood.js';
import {
    initWeapons,
} from './weapons.js';
import {
    checkRollsAndShoot,
} from './utils.js';
import {
    moduleName,
    MODULE,
    debug,
} from './const.js';

export let DEADLANDS_WEAPON = {};
export let DEADLANDS_BLOOD = {};

// register the settings in the init
Hooks.once('init', () => {
    Handlebars.registerHelper('clean', function (aString) {
        return aString.replaceAll("'","");
    });
    registerSettings();
});

// Enable chat message hook after ready
Hooks.once('ready', () => {
    debug(moduleName + ' ready!');
    initWeapons();
    initBlood();
    // Don't enable the hook until the system is ready.  This prevents the
    // module from handling historical chat messages... we only want new ones.
    //betterroll case with hooks
    Hooks.on('renderChatMessage', (message, html, data) => {
        handleMessage(message, html);
    });
});

//Core Swade module case
Hooks.on('swadeAction', (swadeActor, swadeItem, actionType, roll, id) => {
    debug('swadeAction');
    //check the roll and shoot
    checkRollsAndShoot(null, swadeItem, roll, MODULE.SWADECORE, swadeActor);
});

//betterroll case with hooks
Hooks.on('BRSW-RollItem', (message, html) => {
    debug('BRSW-RollItem hook');
    // get actor (Code from betterrolls-swade2)
    let actor = null;
    if (canvas) {
        if (message.getFlag('betterrolls-swade2', 'token')) {
            let token = canvas.tokens.get(message.getFlag('betterrolls-swade2', 'token'));
            if (token) actor = token.actor
        } else {
          // If we couldn't get the token, maybe because it was not defined actor.
          if (message.getFlag('betterrolls-swade2', 'actor')) {
              actor = game.actors.get(message.getFlag('betterrolls-swade2', 'actor'));
          }
      }
    }
    // get item id
    let item_id = message.getFlag('betterrolls-swade2', 'item_id');
    // get item
    let swadeItem = actor.items.find((item) => item.id === item_id);
    // check the roll and shoot
    checkRollsAndShoot(message, swadeItem, message.roll, MODULE.BETTERROLLS, actor);
});

async function handleMessage(message, html) {
    // if is a roll of a skill
    if (message.isRoll && message.getFlag('swade-tools', 'rolltype') === 'skill') {
        debug('handleMessage');
        // get the item id
        let itemId = message.getFlag('swade-tools', 'itemroll');
        // get the token id
        let tokenId = message.getFlag('swade-tools', 'usetoken');
        // get the actor id
        let actorId = message.getFlag('swade-tools', 'useactor');

        // get the item
        let actor = null;
        if (tokenId) {
            actor = canvas.tokens.get(tokenId).actor;
        } else {
            if (actorId) {
                actor = game.actors.get(actorId);
                //swadeItem = game.actors.get(actorId).items.find((item) => item.id === itemId);
            } else {
                // how can i get the item???
                console.log(moduleName + ': unable to find item:  message content ' +
                            message.data.content + ', type ' + message.data.type);
            }
        }

        let swadeItem = actor?.items.find((item) => item.id === itemId);
        if (swadeItem) {
            // check the roll and shoot
            checkRollsAndShoot(message, swadeItem, message.roll, MODULE.SWADETOOLS, actor);
        }
    }
}
